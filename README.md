# George Wanjohi - Software Engineer

## Education

Jomo Kenyatta University of Agriculture and Technology

## Qualification

B.Sc. Information Technology

## Honours

Second Class - Upper Division

## Work Experience

#
#### 1. Quintelcon Ltd 

*Jan 2016 - April 2016*

Built applications to get local businesses online.



    a. Wordpress Development
	
sample [Oremus Bookshop](http://oremusbookshop.com/)

#### 2. Innova Limited 

*April 2016 - April 2018*

Building Financial Markets web applications. Apps are subject to Copyrights by the owners.



    a. ASP.NET MVC 

    b. MSSQL

    c. HTML5 and CSS

    d. angularjs 

    e. Entity Framework

    f. Signalr

    g. ASP.Net Core

    h. jquery

    i. nodejs

    j. Testing (unit tests and Integration Tests)

    h. Deployment on IIS


## linkedin 

 Find my linkedin profile [here](https://www.linkedin.com/in/wanjohigeorge/)

## Interests

1. Functional Programming (F#)
2. Applications Security

## Contact

1. mail- gwanjohim@gmail.com
2. cell - 0707007337

## Hobbies

1. Travelling
2. Music



